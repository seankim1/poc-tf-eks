data "terraform_remote_state" "net" {

  backend = "s3"
  config = {
    bucket = "tf-state-workshop-2641f34b8678dc54"
    region = data.aws_region.current.name
    key    = "terraform/terraform_locks_net.tfstate"
  }
}