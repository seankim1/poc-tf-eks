# TF_VAR_region
variable "region" {
  description = "The name of the AWS Region"
  type        = string
  default     = "ap-northeast-2"
}

variable "profile" {
  description = "The name of the AWS profile in the credentials file"
  type        = string
  default     = "default"
}

variable "cluster-name" {
  description = "The name of the EKS Cluster"
  type        = string
  default     = "mycluster1"
}


variable "eks_version" {
  type    = string
  default = "1.21"
}


variable "stages" {
type=list(string)
default=["tf-setup","net","iam","c9net","cluster","nodeg","cicd","eks-cidr","sampleapp"]
}

variable "stagecount" {
type=number
default=9
}

variable "no-output" {
  description = "The name of the EKS Cluster"
  type        = string
  default     = "secret"
  sensitive   = true
}

variable "keyid" {
  type = string
  default = "62b821db-af33-4f7e-8f42-6b1b6a55e97f"
}